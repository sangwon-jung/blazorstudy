#pragma checksum "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\Pages\Demos\ParentChild\FrmParent.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1d3a0e1d21f4c0a488106cca43acbccbc52c8772"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorApp1.Pages.Demos.ParentChild
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using BlazorApp1;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using BlazorApp1.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\_Imports.razor"
using BlazorApp1.Models;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Demos/ParentChild/FrmParent")]
    public partial class FrmParent : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>FrmParent</h3>\r\n");
            __builder.OpenElement(1, "input");
            __builder.AddAttribute(2, "type", "button");
            __builder.AddAttribute(3, "value", "부모에서 호출");
            __builder.AddAttribute(4, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 6 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\Pages\Demos\ParentChild\FrmParent.razor"
                                               ParentCall

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseElement();
            __builder.AddMarkupContent(5, "\r\n\r\n<hr>\r\n\r\n");
            __builder.OpenComponent<BlazorApp1.Pages.Demos.ParentChild.FrmChild>(6);
            __builder.AddAttribute(7, "FromParent", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 10 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\Pages\Demos\ParentChild\FrmParent.razor"
                      1234

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(8, "OnParentCall", new System.Action(
#nullable restore
#line 10 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\Pages\Demos\ParentChild\FrmParent.razor"
                                          ParentCall

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(9, "PageIndexChanged", new System.Action<System.Int32>(
#nullable restore
#line 10 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\Pages\Demos\ParentChild\FrmParent.razor"
                                                                        PageIndexChanged

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 15 "D:\Source\SD\BlazorStudy\BlazorApp1\BlazorApp1\Pages\Demos\ParentChild\FrmParent.razor"
       
    protected void ParentCall()
    {
        js.InvokeAsync<object>("alert", "호출됨");
    }
    protected void PageIndexChanged(int pageIndex)
    {
        js.InvokeAsync<object>("alert", $"{pageIndex}인덱스 넘어옴");
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime js { get; set; }
    }
}
#pragma warning restore 1591
